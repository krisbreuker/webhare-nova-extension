# WebHare extension for Nova

Experimental WebHare extension for Nova. This extension provides syntax definitions for HareScript and Witty files and a
language client which connects to a WebHare installation.


## Features

This extension provides:

* Syntax highlighting for HareScript and Witty files
* File diagnostics for HareScript and Witty files and supported XML files
* Code actions to automatically add missing LOADLIBs and rmove unused LOADLIBs
* Document formatter for supported XML files
* Documentation popups on hover
* Jump to definition
* A sidebar which can be used to retrieve stack traces
* A few clips for inserting common code fragments


## Installation

* Install the [`dev`](https://gitlab.webhare.com/webhare-opensource/dev) module in WebHare using `wh module get webhare/dev`
* Install [`runkit`](https://gitlab.webhare.com/webhare-opensource/runkit) by cloning the repository and following the README instructions
* Install the [`WebHare`](https://extensions.panic.com/extensions/dev.webhare/dev.webhare.WebHare/) extension in Nova through
  the Extension Library (`Cmd+Shift+2` or _Extensions_ > _Extension Library…_)


## Configuration

The extension can be configured in the extension preferences.

### Options

**Show Documentation Issues**<br>
Check the checkbox to show diagnostics for documentation issues.

**Show Stack Traces For Workspace Only**<br>
Check the checkbox to filter out retrieved stack traces that don't reference files within the current workspace. This setting
can be overridden in project-specific settings.

**Debug Log Level**<br>
The WebHare `lsp` module can log debugging information to the debug log with an `lsp:server` log source value. This setting
can be used to control the amount of information that is being logged, from 0 (log only errors) to 3 (log everything). This
is a project-specific setting.

### Language Server Settings

**WebHare Runkit Path**<br>
The path of the `runkit` binary, defaults to `~/projects/webhare-runkit/bin/runkit`.


## Development

* Clone the [`webhare-language-nova`](https://gitlab.com/krisbreuker/webhare-nova-extension) repository
* Install the dependencies by running `npm install` within the repository
* Start a watch that automatically runs rollup to compile the TypeScript source by running `npm run watch` within the
  repository
* Open the repository directory as a project in Nova
* Activate the project as an extension by choosing _Extensions_ > _Activate Project as Extension_ (if the WebHare extension
  is already installed through the Extension Library, deactivate that extension first)
* Making any changes to the source files will automatically reload the extension
