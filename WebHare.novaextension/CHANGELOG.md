# 0.3.2 – 2024-11-05

## Things that are nice to know
- Don't keep restarting on non-recoverable error
- Fix Witty syntax activation
- Only log to console in dev mode


# 0.3.1 – 2024-01-26

## Things that are nice to know
- Added 'Copy WebHare Resource Path' command


# 0.3.0 – 2024-01-25

## Incompatibilities and deprecations
- Switched to the new language server built into the `dev` module<br>
  For this to work, the `dev` module should be up to date and `runkit` should be installed and working. If `runkit` isn't
  installed into `~/projects/webhare-runkit`, set the path to the `runkit` binary in the extension's settings.

## Things that are nice to know
- Added 'Object comment' clip

## Things that are fixed
- Removed the `window/showDocument` workaround, as Nova supports this natively now


# 0.2.0 - 2023-05-15

## Things that are nice to know
- Add an option to automatically format known XML files on saving
- Add a Run Task for HareScript files

## Things that are fixed
- Improved the detection of the relevant stack trace item, so newer file types (e.g. TypeScript) are recognized


# 0.1.8 - 2022-02-15

## Things that are nice to know
- Add support LSP 3.16's `window/showDocument` request, which is called by the dev module's `OpenReveal` hook
- Add a 'WebHare Preferences' command to open the WebHare extension preferences for the current workspace


# 0.1.7 - 2021-12-13

## Things that are nice to know
- Add 'Organize LOADLIBs' code action for HareScript files
- Add a 'Format Document' command for supported XML files


# 0.1.6 - 2021-05-31

## Things that are nice to know
- Make server debug loglevel configurable
- Switch to using `@webhare/language-server`


# 0.1.5 - 2021-05-21

## Things that are nice to know
- Fix double clicking a stack trace error message
- Fix openFile and getParent implementation based on updated TypeScript types


# 0.1.4 - 2021-05-12

## Things that are nice to know
- Install `webhare-language-server` module locally instead of relying on a globally available `webhare-language-server`
- Add support for XML file diagnostics for supported files
- Syntax highlighting improvements
- Add option to enable/disabled documentation diagnostics


# 0.1.0 - 2021-04-22

Initial release

## Things that are nice to know
* HareScript and Witty syntax highlighting
* HareScript and Witty file diagnostics
* Show stack trace in sidebar
* Add some HareScript and XML code clips
