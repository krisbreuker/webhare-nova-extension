import { debugLog } from "./support";
import { WebHareLanguageServer } from "./webHareLanguageServer";
import { StackTraceParams, StackTraceResponse } from "@webhare/lsp-types";

// The tree item element type
interface Element
{
  readonly id: string;
}

// A single stack trace item (error with stack trace)
interface StackTraceItem
{
  guid: string;
  date: Date;
  error?: string;
  trace: TracePosition[];
  inWorkspace: boolean;
  element: Element;
}

// An item within an error's stack trace
interface TracePosition
{
  primary: boolean;
  editorpath: string;
  filename: string;
  line: number;
  col: number;
  message?: string;
  func?: string;
}

// An error within the list of stack traces and possibly an item within the error's stack trace
interface ErrorAndTrace
{
  error: StackTraceItem;
  trace?: TracePosition;
}

class StackTraceProvider implements TreeDataProvider<Element>, Disposable
{
  private langServer: WebHareLanguageServer | null;
  private traceView: StackTraceView | null;
  private stackTraces: Map<string, StackTraceItem>;
  private lastGuid: string;

  constructor(whLangServer: WebHareLanguageServer, stackTraceView: StackTraceView) {
    this.langServer = whLangServer;
    this.traceView = stackTraceView;
    this.stackTraces = new Map();
    this.lastGuid = "";
  }

  dispose() {
    debugLog("disposing StackTraceProvider");
    this.langServer = null;
    this.traceView = null;
  }

  reset() {
    this.stackTraces.clear();
    this.lastGuid = "";
  }

  getParent(element: Element): Element | null {
    debugLog(`Get parent of ${element.id}`);
    const item = this.getErrorAndTrace(element.id);
    if (item.trace)
      return item.error.element;
    return null;
  }

  getChildren(element: Element): Element[] {
    debugLog(`Get children of ${element ? element.id : "<root>"}`);
    if (!element)
    {
      let items = Array.from(this.stackTraces.values());
      if (items.length && this.traceView?.workspaceTraces) {
        items = items.filter(_ => _.inWorkspace);
      }
      return items.reverse().map(_ => _.element);
    }
    const item = this.stackTraces.get(element.id) || null;
    if (item) {
      return item.trace.map((_, idx) => ({ id: element.id + "." + idx }));
    }
    return [];
  }

  getTreeItem(element: Element): TreeItem {
    debugLog(`Get tree item ${element.id}`);
    const item = this.getErrorAndTrace(element.id);
    if (item.trace) {
      let name: string = `${item.trace.filename} (${item.trace.line}:${item.trace.col})`;
      if (item.trace.message) {
        name += `: ${item.trace.message}`;
      } else if (item.trace.func) {
        name += ` when calling ${item.trace.func}`;
      }
      const treeItem = new TreeItem(name);
      treeItem.command = "webhare.traces.openstacktraceitem";
      treeItem.image = item.trace.message ? "__symbol.expression" : "__symbol.function";
      return treeItem;
    } else {
      let name: string = `${item.error.date.toLocaleString()}: ${item.error.error}`;
      const treeItem = new TreeItem(name, item.error.guid === this.lastGuid ? TreeItemCollapsibleState.Expanded : TreeItemCollapsibleState.Collapsed);
      treeItem.command = "webhare.traces.openstacktraceitem";
      treeItem.image = "__symbol.expression";
      return treeItem;
    }
  }

  openTraceItem(selection: Element[], _transient: boolean) {
    if (selection.length === 1) {
      const item = this.getErrorAndTrace(selection[0].id);
      if (item.trace) {
        debugLog(`Open file ${item.trace.editorpath}#${item.trace.line}#${item.trace.col}`);
        nova.workspace.openFile(item.trace.editorpath, { line: item.trace.line, column: item.trace.col });
      } else {
        const traceId = this.getRelevantTraceId(selection[0].id);
        if (traceId >= 0 && traceId < item.error.trace.length) {
          debugLog(`Open file ${item.error.trace[traceId].editorpath}#${item.error.trace[traceId].line}#${item.error.trace[traceId].col}`);
          nova.workspace.openFile(item.error.trace[traceId].editorpath, { line: item.error.trace[traceId].line, column: item.error.trace[traceId].col });
        }
      }
    }
  }

  async retrieveErrorStackTrace() {
    if (!this.langServer?.languageClient || !nova.workspace) {
      return;
    }
    if (nova.workspace.activeTextEditor?.document.syntax != "harescript") {
      nova.workspace.showInformativeMessage("This function is only available from within HareScript files.");
      return;
    }

    debugLog("Getting stack trace");
    const trace: StackTraceParams = {
      textDocument: { uri: nova.workspace.activeTextEditor.document.uri },
      lastGuid: this.lastGuid
    }
    const res = await this.langServer.languageClient.sendRequest("webHare/getStackTrace", trace) as StackTraceResponse;
    if (!res) {
      return;
    }

    if (res.errors.length) {
      for (const error of res.errors) {
        const element = { id: error.guid };
        for (const item of error.stack) {
          if (item.message) {
            item.message = item.message.split("\n").join(" ");
          }
        }
        this.stackTraces.set(element.id, {
          guid: error.guid,
          date: new Date(error.date),
          error: error.stack[0].message,
          trace: error.stack,
          inWorkspace: error.stack.some((pos: TracePosition) => {
            return nova.workspace.contains(decodeURI(pos.editorpath));
          }),
          element
        });
        this.lastGuid = error.guid;
      }
    }
    debugLog(`Got ${res.errors.length} errors of which ${[...this.stackTraces.values()].filter(_ => _.inWorkspace).length} in workspace`);
  }

  getNewestError(): StackTraceItem | null {
    return Array.from(this.stackTraces.values()).reverse().shift() || null;
  }

  private getRelevantTraceId(id: string): number {
    let error = this.stackTraces.get(id);
    if (error) {
      return error.trace.findIndex(trace => trace.primary);
    }
    return -1;
  }

  private getErrorAndTrace(id: string): ErrorAndTrace {
    const errorId = id.split(".")[0];
    let error: StackTraceItem = this.stackTraces.get(errorId) as StackTraceItem;
    let trace: TracePosition;
    if (error) {
      const traceId = parseInt(id.substring(errorId.length + 1));
      if (traceId >= 0) {
        trace = error.trace[traceId];
        return { error, trace };
      }
    }
    return { error };
  }
}

export class StackTraceView implements Disposable
{
  private _workspaceTraces: boolean;
  private compositeDisposable: CompositeDisposable;
  private traceProvider: StackTraceProvider | null;
  private treeView: TreeView<Element> | null;

  get workspaceTraces() {
    return this._workspaceTraces;
  }

  constructor(whLangServer: WebHareLanguageServer) {
    this._workspaceTraces = false;

    this.compositeDisposable = new CompositeDisposable();

    this.traceProvider = new StackTraceProvider(whLangServer, this);
    this.compositeDisposable.add(this.traceProvider);

    this.treeView = new TreeView("webhare.traces.stacktraces", {
      dataProvider: this.traceProvider,
    });
    this.compositeDisposable.add(this.treeView);

    // Open transient version of file on selection
    this.treeView.onDidChangeSelection((selection: Element[]) => this.openTraceItem(selection, true));

    this.compositeDisposable.add(
      nova.commands.register("webhare.getStackTrace", async () => {
        const have_errors = await this.refresh();
        //ADDME: How to switch to panel?
        if (have_errors && !this.treeView?.visible) {
            nova.workspace.showInformativeMessage("Switch to the Traces tab to view the stack traces.");
        }
      })
    );

    this.compositeDisposable.add(
      nova.commands.register("webhare.traces.getStackTrace", () => {
        this.refresh();
      })
    );

    this.compositeDisposable.add(
      nova.commands.register("webhare.traces.clearStackTraces", () => {
        debugLog("Clear all stack traces");
        this.reset();
      })
    );

    this.compositeDisposable.add(
      nova.commands.register("webhare.traces.openstacktraceitem", () => {
        if (this.treeView) {
          this.openTraceItem(this.treeView.selection, false);
        }
      })
    );

    this.compositeDisposable.add(
      nova.config.observe("webhare.workspace-specific-stacktrace", () => {
        this.checkWorkspaceTraces();
      })
    );

    this.compositeDisposable.add(
      nova.workspace.config.observe("webhare.workspace-specific-stacktrace", () => {
        this.checkWorkspaceTraces();
      })
    );

    this.getWorkspaceTraces();
  }

  dispose() {
    debugLog("disposing StackTraceView");
    this.treeView = null;
    this.traceProvider = null;
    this.compositeDisposable.dispose();
  }

  reset() {
    this.traceProvider?.reset();
    this.reload();
  }

  reload() {
    this.treeView?.reload();
  }

  async refresh(): Promise<boolean> {
    debugLog("Get stack trace");
    await this.traceProvider?.retrieveErrorStackTrace();
    await this.treeView?.reload();
    const error = this.traceProvider?.getNewestError();
    if (error) {
      debugLog(`Revealing ${error.element.id}`);
      this.treeView?.reveal(error.element, { select: true, focus: true, reveal: 1 });
    }
    return !!error;
  }

  openTraceItem(selection: Element[], transient: boolean) {
    this.traceProvider?.openTraceItem(selection, transient)
  }

  private checkWorkspaceTraces() {
    if (this.getWorkspaceTraces()) {
      this.treeView?.reload();
    }
  }

  private getWorkspaceTraces(): boolean {
    let newSetting: boolean;
    const workspaceSetting = nova.workspace.config.get("webhare.workspace-specific-stacktrace", "string");
    if (workspaceSetting === "true") {
      newSetting = true;
    } else if (workspaceSetting === "false") {
      newSetting = false;
    } else {
      newSetting = nova.config.get("webhare.workspace-specific-stacktrace", "boolean") ?? true;
    }
    if (newSetting === this._workspaceTraces) {
      return false;
    }
    this._workspaceTraces = newSetting;
    return true;
  }
}
