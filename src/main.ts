import { debugLog } from "./support";
import { WebHareLanguageServer } from "./webHareLanguageServer";
import { StackTraceView } from "./stackTraceView";

let langServer: WebHareLanguageServer | null = null;

nova.commands.register("webhare.openWorkspaceConfig", openWorkspaceConfig);
nova.commands.register("webhare.reload", reload);
nova.commands.register("webhare.copyResourcePath", copyResourcePath);

export function activate() {
  debugLog("activating...");

  // Initialize a language server
  langServer = new WebHareLanguageServer();

  // Initialize the stack trace view
  const stackTraceView = new StackTraceView(langServer);
  nova.subscriptions.add(stackTraceView);

  // Initialize an on save handler to autoformat XML files on save
  nova.subscriptions.add(nova.workspace.onDidAddTextEditor(editor => {
    const editorDisposable = new CompositeDisposable();
    nova.subscriptions.add(editorDisposable);
    nova.subscriptions.add(editor.onDidDestroy(() => editorDisposable.dispose()));

    editorDisposable.add(
      nova.config.onDidChange("webhare.format-document-on-save", refreshListener)
    );
    editorDisposable.add(
      nova.workspace.config.onDidChange("webhare.format-document-on-save", refreshListener)
    );

    let willSaveListener = setupListener();
    nova.subscriptions.add({
      dispose() {
        willSaveListener?.dispose();
      }
    });

    function refreshListener() {
      willSaveListener?.dispose();
      willSaveListener = setupListener();
    }

    function setupListener() {
      if (editor.document.syntax != "xml") {
        return;
      }

      let formatDocumentOnSave: boolean = false;
      const workspaceSetting = nova.workspace.config.get("webhare.format-document-on-save", "string");
      if (workspaceSetting === "true") {
        formatDocumentOnSave = true;
      } else if (workspaceSetting === "false") {
        formatDocumentOnSave = false;
      } else {
        formatDocumentOnSave = nova.config.get("webhare.format-document-on-save", "boolean") ?? formatDocumentOnSave;
      }
      if (!formatDocumentOnSave) {
        return;
      }
      return editor.onWillSave(async editor => {
        await nova.commands.invoke("webhare.formatDocument", editor);
      });
    }
  }))
}

export function deactivate() {
  debugLog("deactivating...");
  langServer?.deactivate();
}

function openWorkspaceConfig() {
  nova.workspace.openConfig();
}

function reload() {
  deactivate();
  activate();
}

function copyResourcePath(editor: TextEditor) {
  if (!langServer?.languageClient || !editor.document?.path) {
    return;
  }
  langServer.languageClient.sendRequest("webHare/toResourcePath", [editor.document.path]).then((res: unknown) => {
    if (res)
      nova.clipboard.writeText(res as string);
    else
      nova.workspace.showErrorMessage(`Unable to find module for ${editor.document.path}`);
  });
}
