export function debugLog(...args: unknown[]) {
  if (nova.inDevMode())
    console.log(...args);
}

export function debugInfo(...args: unknown[]) {
  if (nova.inDevMode())
    console.info(...args);
}

export function debugWarn(...args: unknown[]) {
  if (nova.inDevMode())
    console.warn(...args);
}

export function debugError(...args: unknown[]) {
  if (nova.inDevMode())
    console.error(...args);
}
