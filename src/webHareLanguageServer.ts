import { debugWarn, debugError } from "./support";
import { initLSPCompatibility } from "./lspCompatibility";
import { WebHareTaskAssistant } from "./taskAssistant";

const DEFAULT_RUNKIT_PATH = nova.path.join(nova.environment.HOME, "projects", "webhare-runkit", "bin", "runkit");
const RESTART_CHECK_PERIOD = 5; // Minutes
const RESTART_CHECK_COUNT = 3;

export class WebHareLanguageServer {
  runkitPath: string;
  languageClient: LanguageClient | null;
  restarts: Date[] = [];

  constructor() {
    this.runkitPath = nova.config.get("webhare.runkit-path", "string") || DEFAULT_RUNKIT_PATH;
    this.languageClient = null;

    // Observe the configuration setting for the custom language server module, and restart the server on change
    nova.subscriptions.add(
      nova.config.observe("webhare.runkit-path", (path: string) => {
        const newSetting = path || DEFAULT_RUNKIT_PATH;
        if (newSetting !== this.runkitPath) {
          this.runkitPath = newSetting;
          this.start();
        }
      })
    );

    nova.subscriptions.add(nova.assistants.registerTaskAssistant(new WebHareTaskAssistant(this), {
      identifier: "webhare.WebHare",
      name: "WebHare"
    }));

    this.start();

    initLSPCompatibility(this);
  }

  deactivate() {
    this.stop();
  }

  start() {
    // Make sure that the custom language server module path exists
    if (!nova.fs.stat(this.runkitPath)) {
      debugWarn(`Language server module '${this.runkitPath}' is not an existing directory`);
      return;
    }

    const args = [ "wh", "dev:languageserver" ];
    //TODO: Make this a workspace-specific setting? But only in dev mode?
    // if (nova.inDevMode()) {
    //   args = [ "--nolazy", "--inspect=6009", ...args ];
    // }

    // Stop a currently running client
    this.stop();

    // Create the client
    const serverOptions = {
      path: this.runkitPath,
      args
    };
    const clientOptions = {
      // The set of document syntaxes for which the server is valid
      syntaxes: [ "harescript", "witty-base", "witty-html", "xml" ]
    };
    const client = new LanguageClient("webhare-langserver", "WebHare Language Server", serverOptions, clientOptions);

    // If the client stops because of an error, restart it
    client.onDidStop((error?: Error) => {
      if (error) {
        // Check if not already restarted too many times within the check period
        const checkPeriod = new Date();
        checkPeriod.setMinutes(checkPeriod.getMinutes() - RESTART_CHECK_PERIOD);
        this.restarts = this.restarts.filter(date => date > checkPeriod);
        this.restarts.push(new Date());
        if (this.restarts.length > RESTART_CHECK_COUNT) {
          //TODO: Is there any way to show a message to the user if the client's not currently running?
          nova.beep();
          debugError(`Not restarting client, it crashed more than ${RESTART_CHECK_COUNT} times in the last ${RESTART_CHECK_PERIOD} minutes`);
        } else {
          debugError("Restarting client because of error", error);
          client.start();
        }
      }
    });

    try {
      // Start the client
      client.start();

      // Add the client to the subscriptions to be cleaned up
      this.languageClient = client;
    }
    catch (err) {
      // If the .start() method throws, it's likely because the path to the language server is invalid
      debugError(err);
    }
  }

  stop() {
    if (this.languageClient) {
      this.languageClient.stop();
      this.languageClient = null;
    }
  }
}
