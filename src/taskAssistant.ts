import { WebHareLanguageServer } from "./webHareLanguageServer";
import { ConfigParams, ConfigResponse } from "@webhare/lsp-types";

export class WebHareTaskAssistant implements TaskAssistant {
  private _langServer: WebHareLanguageServer;

  constructor(server: WebHareLanguageServer) {
    this._langServer = server;
  }

  provideTasks(): AssistantArray<Task> {
    const tasks = [];

    // Return a resolvable task, so we can prevent running run script for file we cannot run
    const runScriptTask = new Task("Run Script");
    runScriptTask.setAction(Task.Run, new TaskResolvableAction({ data: { action: "run" } }))
    tasks.push(runScriptTask);

    return tasks;
  }

  async resolveTaskAction(context: TaskActionResolveContext<any>) {
    if (!this._langServer.languageClient) {
      return null;
    }

    if (context.data?.action === "run") {
      if (!nova.workspace?.activeTextEditor?.document.path
          || !nova.workspace?.activeTextEditor?.document.syntax
          || !["harescript"].includes(nova.workspace.activeTextEditor.document.syntax)) {
        return null;
      }

      // Retrieve the WebHare configuration, so we can set the WEBHARE_DATAROOT environment variable
      const params: ConfigParams = {
        textDocument: { uri: nova.workspace.activeTextEditor.document.uri }
      }
      const config = await this._langServer.languageClient.sendRequest("webHare/getConfig", params) as ConfigResponse;

      return new TaskProcessAction("wh", {
        args: [
          "dev:__sublimerun",
          nova.workspace.activeTextEditor.document.path
        ],
        env: { "WEBHARE_DATAROOT": config.basedataroot },
        shell: true
      });
    }
    return null;
  }
}
