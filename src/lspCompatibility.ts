import { WebHareLanguageServer } from "./webHareLanguageServer";

let langServer: WebHareLanguageServer | null;

// Initialize code that fixes come LSP issues with Nova's language client
export function initLSPCompatibility(whLangServer: WebHareLanguageServer): void {
  langServer = whLangServer;

  nova.subscriptions.add(nova.commands.register("webhare.formatDocument", () => formatCurrentDocument()));
}

// Nova doesn't implement the Document Formatting Request, so we'll implement it as a command
// https://microsoft.github.io/language-server-protocol/specifications/specification-current/#textDocument_formatting
async function formatCurrentDocument() {
  const editor = nova.workspace.activeTextEditor;
  if (!langServer?.languageClient || !editor)
    return;
  if (editor.document.syntax && [ "harescript", "witty-html", "xml" ].includes(editor.document.syntax)) {
    const params = {
      textDocument: { uri: editor.document.uri },
      options: { tabSize: editor.tabLength, insertSpace: !!editor.softTabs }
    };
    const response: any = await langServer.languageClient.sendRequest("textDocument/formatting", params);
    if (response && response.length > 0) {
      editor.edit((edit: TextEditorEdit) => {
        //FIXME: Use the returned range to create a new Range, but then we need to convert line,character to indices within the document
        edit.replace(new Range(0, editor.document.length), response[0].newText);
      });
    }
  }
}
